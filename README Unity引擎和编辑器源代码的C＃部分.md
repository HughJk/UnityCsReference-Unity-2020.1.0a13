## Unity 2020.1.0a13 C# reference source code

The C# part of the Unity engine and editor source code.
May be used for reference purposes only.

For terms of use, see
https://unity3d.com/legal/licenses/Unity_Reference_Only_License

The repository includes third-party code subject to [third-party
notices](third-party-notices.txt).

The terms of use do not permit you to modify or redistribute the C#
code (in either source or binary form). If you want to modify Unity's
source code (C# and C++), contact Unity sales for a commercial source
code license: https://store.unity.com/contact?type=source-code

We do not take pull requests at this time (sorry). But if you find
something that looks like a bug, we'd appreciate it if you'd file it
using the Unity Bug Reporter. For more information, see our blog post:
https://blogs.unity3d.com/2018/03/26/releasing-the-unity-c-source-code/

Unless expressly provided otherwise, the software under this
license is made available strictly on an "AS IS" BASIS WITHOUT
WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. Please review the
license for details on these and other terms and conditions.

The C# solution is in Projects/CSharp/UnityReferenceSource.sln

The folder and file layout of the reference source matches
the Unity source tree layout; it can and will change between
different Unity versions.


1293/5000
Unity引擎和编辑器源代码的C＃部分。
仅可用于参考目的。

有关使用条款，请参阅
https://unity3d.com/legal/licenses/Unity_Reference_Only_License

该存储库包含受[第三方
声明]（third-party-notices.txt）。

使用条款不允许您修改或重新分发C＃
代码（源或二进制形式）。如果您想修改Unity的
源代码（C＃和C ++），请联系Unity销售以获取商业信息
代码许可证：https://store.unity.com/contact?type=source-code

我们目前不接受请求（抱歉）。但是如果你发现
看起来像错误的东西，如果您将其归档，我们将不胜感激
使用Unity Bug Reporter。有关更多信息，请参见我们的博客文章：
https://blogs.unity3d.com/2018/03/26/releasing-the-unity-c-source-code/

除非另有明确规定，否则本协议下的软件
许可严格按“原样”提供，不提供
任何形式的明示或暗示的担保。请检查
这些和其他条款和条件的详细信息的许可。

C＃解决方案位于Projects / CSharp / UnityReferenceSource.sln中

参考源的文件夹和文件布局匹配
Unity源代码树布局；它可以并且将会在
不同的Unity版本。